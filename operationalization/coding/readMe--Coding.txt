# Subdirectory: Coder-agreement
You may want to calculate agreement between 2 or more coders (inter-coder agreement) or between the earlier and later work of a single coder (intra-coder agreement). You can use, for example, measurements that do not take "agreement by chance" into consideration, such as percentage agreement, or you can use measurements that do, such as Cohen's Kappa.

This tool helps you calculate both types of agreement:
http://dfreelon.org/utils/recalfront/recal2

This tool is for Cohen's Kappa only:
https://idostatistics.com/cohen-kappa-free-calculator


# Subdirectory: Coder-training
One or multiple coders may be performing coding; in both cases, it may be beneficial to make note of how coding should be performed. For multiple coders, there may be a training to educate coders and help them practice coding subsets of the data. This subdirectory is for such instructions, materials, and reflections on the process.

# Subdirectory: Final-codebook
You may want to have a "living codebook" until the end of the coding process, even after you have finalized the codes (but continue to refine definitions and/or add examples). In this case, the living document can be downloaded (e.g., from a collaborative space) in various stages and placed into this subdirectory. And/or the final version of the codebook can be housed here.


# Analytical and coding tools
See here: SQAFFOLD\operationalization\analysis-plans --> "readMe--Analysis_plans" file