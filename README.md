# Comparing Self-Perceived Digital Health Literacy Skills with Stimulus-Engagement Alignment among Dutch Individuals

The study examines Dutch-speaking individuals living in the Netherlands and aims to 1) Gain a deeper understanding of online health information appraisal to develop an intervention that increases digital health literacy and 2) Compare participants’ self-perceived digital health literacy skills with stimulus-engagement alignment (a technique comparing stimulus-based aspects of online content with those engaged by the participant).

Our research questions (RQs) are the following:
RQ 1: What features of trustworthiness do participants identify as indicating trustworthiness or untrustworthiness of online content?
RQ 2: What strategies are identified to assess the trustworthiness or untrustworthiness of online content?
RQ 3: How do features and strategies reported by individuals differ from aspects of the stimulus they engaged with (i.e., stimulus-engagement alignment)  based on subgroups created by age, sex, education, and socio-economic position?
RQ 4: How does stimulus-engagement alignment differ from participants’ self-perceived digital health literacy skills?
